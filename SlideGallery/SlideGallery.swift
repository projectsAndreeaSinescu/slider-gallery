//
//  ViewController.swift
//  SlideGallery
//
//  Created by Andreea Sinescu on 06/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit

class SlideGallery: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var fixedImageView: UIImageView!
    @IBOutlet weak var swipeLeftImageView: UIImageView!
    @IBOutlet weak var widthLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var swipeLeftView: UIView!
    @IBOutlet weak var swipeRightImageView: UIImageView!
    @IBOutlet weak var widthRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightBlurView: UIVisualEffectView!
    @IBOutlet weak var leftBlurView: UIVisualEffectView!
    @IBOutlet weak var swipeRightView: UIView!

    var pictures: [UIImage] = []
    var currentImageIndex = 0
    
    static func createView(images: [UIImage], index: Int) -> SlideGallery {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "SlideGallery") as! SlideGallery
        controller.pictures = images
        controller.currentImageIndex = index
        return controller
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pageControl.numberOfPages = pictures.count
        pageControl.currentPage = currentImageIndex
        fixedImageView.image = pictures[currentImageIndex]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func handleEdgePanLeft(gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        if gestureRecognizer.state == .began {
            if currentImageIndex != 0 {
                leftBlurView.alpha = 1
                swipeLeftImageView.image = pictures[currentImageIndex - 1]
            }
        }
        if gestureRecognizer.state == .changed {
            if gestureRecognizer.edges == .left && currentImageIndex != 0 {
                widthLeftConstraint.constant = gestureRecognizer.location(in: self.view).x
                leftBlurView.alpha = (1 - widthLeftConstraint.constant / view.frame.size.width) * 1.5
                if widthLeftConstraint.constant >= view.frame.size.width / 2 && pageControl.currentPage != currentImageIndex - 1 {
                    pageControl.currentPage = currentImageIndex - 1
                } else if widthLeftConstraint.constant < view.frame.size.width / 2 && pageControl.currentPage != currentImageIndex {
                    pageControl.currentPage = currentImageIndex
                }
            }
        }
        if gestureRecognizer.state == .ended {
            if currentImageIndex == 0 {
                currentImageIndex = 0
            } else if widthLeftConstraint.constant < view.frame.size.width / 2 {
                UIView.animate(withDuration: 0.2) {
                    self.widthLeftConstraint.constant = 0
                    self.view.layoutIfNeeded()
                    self.leftBlurView.alpha = self.leftBlurView.alpha + 0.5
                }
            } else {
                currentImageIndex = currentImageIndex - 1
                UIView.animate(withDuration: 0.2, animations: {
                    self.widthLeftConstraint.constant = self.view.frame.size.width
                    self.leftBlurView.alpha = 0
                    self.view.layoutIfNeeded()
                }) {
                    (_) in
                    self.fixedImageView.image = self.swipeLeftImageView.image
                    self.widthLeftConstraint.constant = 0
                }
            }
        }
    }
    
    @IBAction func handleEdgePanRight(gestureRecognizer: UIScreenEdgePanGestureRecognizer) {
        if gestureRecognizer.state == .began {
            if currentImageIndex != pictures.count - 1 {
                rightBlurView.alpha = 0
                swipeRightImageView.image = pictures[currentImageIndex + 1]
            }
        }
        if gestureRecognizer.state == .changed {
            if gestureRecognizer.edges == .right && currentImageIndex != pictures.count - 1 {
                widthRightConstraint.constant = view.frame.size.width -  gestureRecognizer.location(in: self.view).x
                rightBlurView.alpha = widthRightConstraint.constant / view.frame.size.width * 1.5
                if widthRightConstraint.constant < view.frame.size.width / 2 && pageControl.currentPage != currentImageIndex {
                    pageControl.currentPage = currentImageIndex
                } else if widthRightConstraint.constant >= view.frame.size.width / 2 && pageControl.currentPage != currentImageIndex + 1 {
                    pageControl.currentPage = currentImageIndex + 1
                }
            }
        }
        if gestureRecognizer.state == .ended {
            if currentImageIndex == pictures.count - 1 {
                currentImageIndex = pictures.count - 1
            } else if widthRightConstraint.constant >= view.frame.size.width / 2 {
                currentImageIndex = currentImageIndex + 1
                UIView.animate(withDuration: 0.2, animations: {
                    self.widthRightConstraint.constant = self.view.frame.size.width
                    self.rightBlurView.alpha += 0.5
                    self.view.layoutIfNeeded()
                }) {
                    (_) in
                    self.rightBlurView.alpha = 0
                    self.fixedImageView.image = self.swipeRightImageView.image
                    self.widthRightConstraint.constant = 0
                }
            } else {
                UIView.animate(withDuration: 0.2, animations: {
                    self.widthRightConstraint.constant = 0
                    self.rightBlurView.alpha = 0
                    self.view.layoutIfNeeded()
                })
            }
        }
    }
}

