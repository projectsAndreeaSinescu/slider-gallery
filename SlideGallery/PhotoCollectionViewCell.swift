//
//  PhotoCollectionViewCell.swift
//  SlideGallery
//
//  Created by Andreea Sinescu on 06/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    func createCell(image: UIImage?) {
        if let _ = image {
            imageView.image = image
        }
    }
}
