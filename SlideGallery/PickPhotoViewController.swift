//
//  PickPhotoViewController.swift
//  SlideGallery
//
//  Created by Andreea Sinescu on 06/08/2018.
//  Copyright © 2018 Andreea Sinescu. All rights reserved.
//

import UIKit

class PickPhotoViewController: UIViewController, HSMediaPickerDelegate, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var pictures: [UIImage] = []
    var pickPhotoPicker: HSMediaPicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Pick photo
    @IBAction func pickPhoto() {
        pickPhotoPicker = HSMediaPicker(controller: self, delegate: self)
        pickPhotoPicker.presentImagePicker(from: .camera, media: .image, allowEditing: false)
    }

    func didCancel() {
        dismiss(animated: true, completion: nil)
    }
    
    func didFinishPickingMediaWithInfo(info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        if let _ = image {
            pictures = pictures.reversed()
            pictures.append(image!)
            pictures = pictures.reversed()
            let indexPath = IndexPath(row: 0, section: 0)
            collectionView.insertItems(at: [indexPath])
            collectionView.reloadItems(at: [indexPath])
            collectionView.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition.left, animated: false)
        }
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:- CollectionDataSource functions
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictures.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        cell.createCell(image: pictures[indexPath.row])
        let gestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        cell.addGestureRecognizer(gestureRecognizer)
        return cell
    }
    
    //MARK:- Select item function
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let controller = SlideGallery.createView(images: pictures, index: indexPath.row)
        self.present(controller, animated: true, completion: nil)
    }
    
    //MARK:- Actions
    @IBAction func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state == .began {
            let alert = UIAlertController(title: "Delete", message: "Are you sure?", preferredStyle: .actionSheet)
            let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let actionYes = UIAlertAction(title: "YES", style: .destructive) { (_) in
                let cell = gestureRecognizer.view! as! PhotoCollectionViewCell
                let indexPath = self.collectionView.indexPath(for: cell)
                self.pictures.remove(at: indexPath!.row)
                self.collectionView.deleteItems(at: [indexPath!])
            }
            alert.addAction(actionYes)
            alert.addAction(actionCancel)
            
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}
